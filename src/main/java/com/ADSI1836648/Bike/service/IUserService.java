package com.ADSI1836648.Bike.service;

import com.ADSI1836648.Bike.domain.Users;

public interface IUserService {

    public Users save(Users user);

    public Iterable<Users> getAll();
}
